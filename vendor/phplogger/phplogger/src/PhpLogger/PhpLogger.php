<?php
/**
 * Created by PhpStorm.
 * User: Bogdans
 * Date: 8/5/2018
 * Time: 2:42 PM
 */

namespace PhpLogger;

/**
 * Class PhpLogger
 * @package PhpLogger
 * @deprecated left for backwards compatibility. will be removed in the next major release
 */
class PhpLogger extends Logger
{
}