<?php
/* μlogger
 *
 * Copyright(C) 2017 Bartek Fabiszewski (www.fabiszewski.net)
 *
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

// default language for translations

// strings only used in setup
$langSetup["dbconnectfailed"] = "Falha na conexão do banco de dados.";
$langSetup["serversaid"] = "Servidor disse: %s"; // substitutes server error message
$langSetup["checkdbsettings"] = "Por favor, verifique as configurações do banco de dados no arquivo 'config. php'.";
$langSetup["dbqueryfailed"] = "Falha na consulta de banco de dados.";
$langSetup["dbtablessuccess"] = "Tabelas de banco de dados criadas com sucesso!";
$langSetup["setupuser"] = "Agora, por favor, configure seu usuário cartorio tracker.";
$langSetup["congratulations"] = "Parabéns!";
$langSetup["setupcomplete"] = "A instalação agora está concluída. Você pode ir para a <a href=\"../index.php\">pagina principal</a> e faça login com sua nova conta de usuário.";
$langSetup["disablewarn"] = "Importante! Você deve desativar SCRIPT 'setup.php' ou removê-lo do seu servidor.";
$langSetup["disabledesc"] = "Deixar o script acessível a partir do navegador é um grande risco de segurança. Qualquer pessoa será capaz de executá-lo, excluir seu banco de dados e configurar nova conta de usuário. Exclua o arquivo ou desabilite-o definindo o valor de% s de volta para %s."; // substitutes variable name and value
$langSetup["setupfailed"] = "Infelizmente algo deu errado. Você pode tentar encontrar mais informação em seus logs do Web Server.";
$langSetup["welcome"] = "Bem-vindo ao Cartorio Tracker!";
$langSetup["disabledwarn"] = "Por motivos de segurança, esse script é desabilitado por padrão. Para habilitá-lo você deve editar 'scripts/setup.php' arquivo no editor de texto e definir %s variável no início do arquivo para %s."; // substitutes variable name and value
$langSetup["lineshouldread"] = "Linha: %s deve ler: %s";
$langSetup["passfuncwarn"] = "Sua versão PHP não suporta funções de senha que acompanham o PHP 5,5. Você tem que incluir password_compat biblioteca.";
$langSetup["passfunchack"] = "Por favor, edite o arquivo 'helpers/user.php' linha Descomente incluindo 'helpers/password.php'.";
$langSetup["dorestart"] = "Por favor, reinicie este script quando tiver terminado.";
$langSetup["createconfig"] = "Por favor, crie o arquivo 'config.php' na pasta raiz. Você pode começar copiando-o de 'config.default.php'. Certifique-se de ajustar os valores de configuração para corresponder às suas necessidades e à configuração do banco de dados.";
$langSetup["nodbsettings"] = "Você deve fornecer suas credenciais de banco de dados em no arquivo 'config.php' (%s)."; // substitutes variable names
$langSetup["scriptdesc"] = "Este script irá configurar tabelas necessárias para Cartorio Tracker (%s). Eles serão criados em seu banco de dados denominado %s. aviso, se as tabelas já existirem, elas serão descartadas e recriados, seu conteúdo será destruído."; // substitutes table names and db name
$langSetup["scriptdesc2"] = "Quando feito o script pedirá que você forneça o nome de usuário e a senha para seu usuário do Cartorio Tracker.";
$langSetup["startbutton"] = "Clique para iniciar";
$langSetup["restartbutton"] = "Reiniciar";
$langSetup["optionwarn"] = "A opção de configuração do PHP %s deve ser definida como %s."; // substitutes option name and value
$langSetup["extensionwarn"] = "Extensão PHP necessária %s não está disponível."; // substitutes extension name


// application strings
$lang["title"] = "• Cartorio Tracker •";
$lang["private"] = "Você precisa de um nome de usuário e senha para acessar esta página.";
$lang["authfail"] = "Usuário ou senha incorreta";
$lang["user"] = "Usuario";
$lang["track"] = "Rastreamento";
$lang["latest"] = "Última posição";
$lang["autoreload"] = "autorecarga";
$lang["reload"] = "Recarregar agora";
$lang["export"] = "Descargar datos";
$lang["chart"] = "Gráfico de altitudes";
$lang["close"] = "cerrar";
$lang["time"] = "Hora";
$lang["speed"] = "Velocidad";
$lang["accuracy"] = "Precisión";
$lang["altitude"] = "Altitud";
$lang["ttime"] = "Tiempo total";
$lang["aspeed"] = "Velocidad media";
$lang["tdistance"] = "Distancia total";
$lang["pointof"] = "Punto %d de %d"; // e.g. Point 3 of 10
$lang["summary"] = "Resumen del viaje";
$lang["suser"] = "seleccione usuario";
$lang["logout"] = "Cerrar sesión";
$lang["login"] = "Identificarse";
$lang["username"] = "Nombre de usuario";
$lang["password"] = "Contraseña";
$lang["language"] = "Lenguaje";
$lang["newinterval"] = "Introduzca nuevo valor para el intervalo (segundos)";
$lang["api"] = "Mapa API";
$lang["units"] = "Unidades";
$lang["metric"] = "Metricas";
$lang["imperial"] = "Imperiales/US";
$lang["nautical"] = "Nautical";
$lang["adminmenu"] = "Administración";
$lang["passwordrepeat"] = "Repita contraseña";
$lang["passwordenter"] = "Introduzca contraseña";
$lang["usernameenter"] = "Introduzca nombre de usuario";
$lang["adduser"] = "Añadir usuario";
$lang["userexists"] = "Ususario ya existe";
$lang["cancel"] ="Cancelar";
$lang["submit"] = "Enviar";
$lang["oldpassword"] = "Contraseña vieja";
$lang["newpassword"] = "Nueva contraseña";
$lang["newpasswordrepeat"] = "Repita nueva contraseña";
$lang["changepass"] = "Cambiar contraseña";
$lang["gps"] = "GPS";
$lang["network"] = "Red";
$lang["deluser"] = "Eliminar usuario";
$lang["edituser"] = "Editar usuario";
$lang["servererror"] = "Error del servidor";
$lang["allrequired"] = "Todos los campos son necesarios";
$lang["passnotmatch"] = "Las contraseñas no coinciden";
$lang["actionsuccess"] = "Acción completada correctamente";
$lang["actionfailure"] = "Ha ocurrido un error";
$lang["userdelwarn"] = "Precaución!\n\nVas a eliminar permanentemente al usuario %s, junto con todas sus rutas y posiciones.\n\n¿Estás seguro?"; // substitutes user login
$lang["editinguser"] = "Estás editando el usuario %s"; // substitutes user login
$lang["selfeditwarn"] = "No puedes editar tu propio usuario";
$lang["apifailure"] = "Upss, no se pueda cargar la API %s"; // substitutes api name (gmaps or openlayers)
$lang["trackdelwarn"] = "Precaución!\n\nVas a eliminar permanentemente la ruta %s y todas sus posiciones.\n\n¿Estás seguro?"; // substitutes track name
$lang["editingtrack"] = "Estás editando la ruta %s"; // substitutes track name
$lang["deltrack"] = "Eliminar ruta";
$lang["trackname"] = "Nombre de ruta";
$lang["edittrack"] = "Editar ruta";
$lang["passlenmin"] = "La contraseña debe tener al menos %d caracteres"; // substitutes password minimum length
$lang["passrules_1"] = "Debe contener al menos una letra minúscula y una mayúscula.";
$lang["passrules_2"] = "Debe contener al menos una letra minúscula, una mayúscula y un número";
$lang["passrules_3"] = "Debe contener al menos una letra minúscula, una mayúscula, un número y un caracter no alfanumérico";
$lang["owntrackswarn"] = "Solo puedes editar tus propias rutas";
$lang["gmauthfailure"] = "Es posible que haya un problema con la clave de la API de Google Maps";
$lang["gmapilink"] = "Puedes encontrar más información sobre las claves de API en <a target=\"_blank\" href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\">esta página de Google</a>";
$lang["import"] = "Importar ruta";
$lang["iuploadfailure"] = "Ha ocurrido un error en la carga";
$lang["iparsefailure"] = "Ha ocurrido un error en el análisis";
$lang["idatafailure"] = "No hay datos de ruta en el archivo importado";
$lang["isizefailure"] = "EL tamaño del archivo no debe superar los %d bytes"; // substitutes number of bytes
$lang["imultiple"] = "Varias rutas importadas (%d)"; // substitutes number of imported tracks
$lang["allusers"] = "Todos los usuarios";
?>
