<?php
/* μlogger
 *
 * Copyright(C) 2017 Bartek Fabiszewski (www.fabiszewski.net)
 *
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

// default language for translations

// strings only used in setup
$langSetup["dbconnectfailed"] = "Falha na conexão do banco de dados.";
$langSetup["serversaid"] = "Servidor disse: %s"; // substitutes server error message
$langSetup["checkdbsettings"] = "Por favor, verifique as configurações do banco de dados no arquivo 'config. php'.";
$langSetup["dbqueryfailed"] = "Falha na consulta de banco de dados.";
$langSetup["dbtablessuccess"] = "Tabelas de banco de dados criadas com sucesso!";
$langSetup["setupuser"] = "Agora, por favor, configure seu usuário cartorio tracker.";
$langSetup["congratulations"] = "Parabéns!";
$langSetup["setupcomplete"] = "A instalação agora está concluída. Você pode ir para a <a href=\"../index.php\">pagina principal</a> e faça login com sua nova conta de usuário.";
$langSetup["disablewarn"] = "Importante! Você deve desativar SCRIPT 'setup.php' ou removê-lo do seu servidor.";
$langSetup["disabledesc"] = "Deixar o script acessível a partir do navegador é um grande risco de segurança. Qualquer pessoa será capaz de executá-lo, excluir seu banco de dados e configurar nova conta de usuário. Exclua o arquivo ou desabilite-o definindo o valor de% s de volta para %s."; // substitutes variable name and value
$langSetup["setupfailed"] = "Infelizmente algo deu errado. Você pode tentar encontrar mais informação em seus logs do Web Server.";
$langSetup["welcome"] = "Bem-vindo ao Cartorio Tracker!";
$langSetup["disabledwarn"] = "Por motivos de segurança, esse script é desabilitado por padrão. Para habilitá-lo você deve editar 'scripts/setup.php' arquivo no editor de texto e definir %s variável no início do arquivo para %s."; // substitutes variable name and value
$langSetup["lineshouldread"] = "Linha: %s deve ler: %s";
$langSetup["passfuncwarn"] = "Sua versão PHP não suporta funções de senha que acompanham o PHP 5,5. Você tem que incluir password_compat biblioteca.";
$langSetup["passfunchack"] = "Por favor, edite o arquivo 'helpers/user.php' linha Descomente incluindo 'helpers/password.php'.";
$langSetup["dorestart"] = "Por favor, reinicie este script quando tiver terminado.";
$langSetup["createconfig"] = "Por favor, crie o arquivo 'config.php' na pasta raiz. Você pode começar copiando-o de 'config.default.php'. Certifique-se de ajustar os valores de configuração para corresponder às suas necessidades e à configuração do banco de dados.";
$langSetup["nodbsettings"] = "Você deve fornecer suas credenciais de banco de dados em no arquivo 'config.php' (%s)."; // substitutes variable names
$langSetup["scriptdesc"] = "Este script irá configurar tabelas necessárias para Cartorio Tracker (%s). Eles serão criados em seu banco de dados denominado %s. aviso, se as tabelas já existirem, elas serão descartadas e recriados, seu conteúdo será destruído."; // substitutes table names and db name
$langSetup["scriptdesc2"] = "Quando feito o script pedirá que você forneça o nome de usuário e a senha para seu usuário do Cartorio Tracker.";
$langSetup["startbutton"] = "Clique para iniciar";
$langSetup["restartbutton"] = "Reiniciar";
$langSetup["optionwarn"] = "A opção de configuração do PHP %s deve ser definida como %s."; // substitutes option name and value
$langSetup["extensionwarn"] = "Extensão PHP necessária %s não está disponível."; // substitutes extension name


// application strings
$lang["title"] = "• Cartorio Tracker •";
$lang["private"] = "Você precisa de um nome de usuário e senha para acessar esta página.";
$lang["authfail"] = "Usuário ou senha incorreta";
$lang["user"] = "Usuario";
$lang["track"] = "Rastreamento";
$lang["start"] = "KM Início";
$lang["end"] = "KM Final";
$lang["latest"] = "Última posição";
$lang["autoreload"] = "autorecarga";
$lang["reload"] = "Recarregar agora";
$lang["export"] = "Transferir dados";
$lang["chart"] = "Gráfico de altitudes";
$lang["close"] = "fechar";
$lang["time"] = "Hora";
$lang["speed"] = "Velocidade";
$lang["accuracy"] = "Precisão";
$lang["altitude"] = "Altitude";
$lang["ttime"] = "Tempo total";
$lang["aspeed"] = "Velocidade média";
$lang["tdistance"] = "Distância total";
$lang["pointof"] = "Ponto %d de %d"; // e.g. Point 3 of 10
$lang["summary"] = "Resumo da viagem";
$lang["suser"] = "selecione o usuario";
$lang["logout"] = "Encerrar sessão";
$lang["login"] = "Identificar";
$lang["username"] = "Nome de usuário";
$lang["password"] = "Senha";
$lang["language"] = "Linguagem";
$lang["newinterval"] = "Insira o novo valor para o intervalo (segundos)";
$lang["api"] = "Mapa API";
$lang["units"] = "Unidades";
$lang["metric"] = "Métricas";
$lang["imperial"] = "Imperiales/US";
$lang["nautical"] = "Náutico";
$lang["adminmenu"] = "Administração";
$lang["passwordrepeat"] = "Repita a senha";
$lang["passwordenter"] = "Insira a senha";
$lang["usernameenter"] = "Digite o nome do usuário";
$lang["adduser"] = "Adicionar usuário";
$lang["userexists"] = "O usuário já existe";
$lang["cancel"] ="Cancelar";
$lang["submit"] = "Enviar";
$lang["oldpassword"] = "Senha antiga";
$lang["newpassword"] = "Nova senha";
$lang["newpasswordrepeat"] = "Repita a nova senha";
$lang["changepass"] = "Trocar senha";
$lang["gps"] = "GPS";
$lang["network"] = "Vermelho";
$lang["deluser"] = "Remover usuário";
$lang["edituser"] = "Editar usuário";
$lang["servererror"] = "Erro do servidor";
$lang["allrequired"] = "Todos os campos são obrigatórios";
$lang["passnotmatch"] = "As senhas não correspondem";
$lang["actionsuccess"] = "Ação concluída com êxito";
$lang["actionfailure"] = "Ocorreu um erro";
$lang["userdelwarn"] = "Cuidado!\n\nVocê excluirá permanentemente o usuário %s, juntamente com todas as suas rotas e posições.\n\nTem certeza?"; // substitutes user login
$lang["editinguser"] = "Estás editando o usuario %s"; // substitutes user login
$lang["selfeditwarn"] = "Você não pode editar seu próprio usuário";
$lang["apifailure"] = "Ops, a API não pode ser carregada %s"; // substitutes api name (gmaps or openlayers)
$lang["trackdelwarn"] = "Cuidado!\n\nVocê vai excluir permanentemente a rota %s e todas as suas posições.\n\nTem certeza?"; // substitutes track name
$lang["editingtrack"] = "Você está editando a rota %s"; // substitutes track name
$lang["deltrack"] = "Eliminar rota";
$lang["trackname"] = "Nome da rota";
$lang["edittrack"] = "Editar rota";
$lang["passlenmin"] = "A senha deve ter pelo menos %d caracteres"; // substitutes password minimum length
$lang["passrules_1"] = "Deve conter pelo menos uma letra minúscula e uma maiúscula.";
$lang["passrules_2"] = "Deve conter pelo menos uma letra minúscula, uma maiúscula e um número";
$lang["passrules_3"] = "Deve conter pelo menos uma letra minúscula, uma maiúscula, um número e um caractere não alfanumérico";
$lang["owntrackswarn"] = "Você só pode editar suas próprias rotas";
$lang["gmauthfailure"] = "Pode haver um problema com a chave da API do Google Maps";
$lang["gmapilink"] = "Você pode encontrar mais informações sobre as chaves de API em <a target=\"_blank\" href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\">esta página de Google</a>";
$lang["import"] = "Importar rota";
$lang["iuploadfailure"] = "Ocorreu um erro de carga";
$lang["iparsefailure"] = "Ocorreu um erro na análise";
$lang["idatafailure"] = "Nenhum dado de caminho no arquivo importado";
$lang["isizefailure"] = "O tamanho do arquivo não deve exceder %d bytes"; // substitutes number of bytes
$lang["imultiple"] = "Varias rotas importadas (%d)"; // substitutes number of imported tracks
$lang["allusers"] = "Todos os usuarios";
?>
